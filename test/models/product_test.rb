require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  def setup
    @product = Product.new( nombre: "Celular",
                            categoria: "Categoria_1",
                            marca: "marca_1",
                            modelo: "modelo_1",
                            observacion:"texto_1",
                            p_compra: 10.5,
                            p_venta: 15.5,
                            cantidad: 10,
                            cod_barra: "codigo_barra",
                            cod_qr: "codigo_qr",
                            ubicacion: "lugar_1" )
  end

  test "should be valid" do
    assert @product.valid?
  end

  test "nombre should be present" do
    @product.nombre = "   "
    assert_not @product.valid?
  end

  test "nombre should be at most 20 characters" do
    @product.nombre = "a" * 21
    assert_not @product.valid?
  end

  test "categoria should be present" do
    @product.categoria = "   "
    assert_not @product.valid?
  end

  test "categoria should be at most 20 characters" do
    @product.categoria = "a" * 21
    assert_not @product.valid?
  end

  test "marca should be present" do
    @product.marca = "   "
    assert_not @product.valid?
  end

  test "marca should be at most 20 characters" do
    @product.marca = "a" * 21
    assert_not @product.valid?
  end

  test "modelo should be present" do
    @product.modelo = "   "
    assert_not @product.valid?
  end

  test "modelo should be at most 20 characters" do
    @product.modelo = "a" * 21
    assert_not @product.valid?
  end

  test "cantidad should be present" do
    @product.cantidad = "   "
    assert_not @product.valid?
  end

  test "ubicacion should be present" do
    @product.ubicacion = "   "
    assert_not @product.valid?
  end

  test "ubicacion should be at most 20 characters" do
    @product.ubicacion = "a" * 21
    assert_not @product.valid?
  end
end
