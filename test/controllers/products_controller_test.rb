require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  def setup
    @product = Product.new( nombre: "Celular",
                            categoria: "Categoria_1",
                            marca: "marca_1",
                            modelo: "modelo_1",
                            observacion:"texto_1",
                            p_compra: 10.5,
                            p_venta: 15.5,
                            cantidad: 10,
                            cod_barra: "codigo_barra",
                            cod_qr: "codigo_qr",
                            ubicacion: "lugar_1" )
  end

end
