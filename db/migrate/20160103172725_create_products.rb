class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :nombre
      t.string :categoria
      t.string :marca
      t.string :modelo
      t.text :observacion
      t.decimal :p_compra, precision: 10, scale: 2
      t.decimal :p_venta, precision: 10, scale: 2
      t.integer :cantidad
      t.string :cod_barra
      t.string :cod_qr
      t.string :ubicacion

      t.timestamps null: false
    end
  end
end
