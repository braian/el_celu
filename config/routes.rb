Rails.application.routes.draw do
  
  root 'products#index'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'sessions/new'

  get 'signup'  => 'users#new'

  resources :products

  resources :users

end
