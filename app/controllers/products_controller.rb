class ProductsController < ApplicationController
  before_action :find_product, only: [:show, :edit, :update, :destroy]

  def show
    @product = Product.find(params[:id])
  end

  def index
    @q = Product.ransack(params[:q])
    @products = @q.result(distinct: true).paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.create(product_params)
    respond_to do |format|
      if @product.save
        format.js
      else
        format.json { render json: @product.errors, status: :unprocessable_entity }
        format.html { render action: "create" }
        format.js
      end
    end
  end

  def edit
  end

  def update
    if @product.update(product_params)
			redirect_to product_path
		else
      render 'edit'
		end
  end

  def destroy
    @product.destroy
    redirect_to :back
  end

  private

  def product_params
    params.require(:product).permit(:nombre,
                                    :categoria,
                                    :marca,
                                    :modelo,
                                    :observacion,
                                    :p_compra,
                                    :p_venta,
                                    :cantidad,
                                    :cod_barra,
                                    :cod_qr,
                                    :ubicacion)
  end

  def find_product
			@product = Product.find_by(id: params[:id])

		if !@product
			redirect_to root_path
			flash[:notice] =  "Lo sentimos. No se pudo procesar su solicitud."
		end
	end

end
