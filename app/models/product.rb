class Product < ActiveRecord::Base

  validates :nombre, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }
  validates :categoria, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }
  validates :marca, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }
  validates :modelo, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }
  validates :cantidad, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }
  validates :ubicacion, length: { maximum: 20 }, :presence => { message: "No puede dejarse vacío" }

  before_save :set_bar_code

  private

  def set_bar_code
    if self.cod_barra.blank?
      self.cod_barra = self.id.to_s + '0' * 3 + Time.now.to_i.to_s
    end
  end

end
