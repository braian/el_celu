$('#products').append("<%= j render(:partial => 'product', locals: { product: @product} ) %>");
$('#myModal').modal('hide');
###
$('#MyModal').on('hide.bs.modal', function () {
    $(this).find('product-form').trigger('reset');
})
###
